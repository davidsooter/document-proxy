package com.cpars;


import com.sap.ecm.api.AbstractCmisProxyServlet;


public class CMISProxySerlet extends AbstractCmisProxyServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected boolean supportAtomPubBinding() {
        return false;
    }

    @Override
    protected boolean supportBrowserBinding() {
        return true;
    }


    public CMISProxyServlet() {
        super();
    }

    @Override
    protected String getRepositoryUniqueName() {
        return "MySampleRepository";
    }

    @Override
    // For applications in production, use a secure location to store the secret key.
    protected String getRepositoryKey() {
        return "abcdef0123456789";
    }

}